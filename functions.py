import numpy as np
import sympy as sp


#  funckja zwracająca wartość podanej funkcji dla danego argumentu x
def functions_value(x, function_number):
    if function_number == 1:
        value = 2 * (np.power(x, 2)) - 2
    elif function_number == 2:
        value = np.sin(x)
    elif function_number == 3:
        value = np.cos(2 * np.power(x, 2) + 1)
    elif function_number == 4:
        value = np.abs(x)
    else:
        value = None
    return value


def horner(factors, x):
    result = factors[0]
    for a in range(1, len(factors)):
        result = result * x + factors[a]
    return result
