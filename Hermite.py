from functions import functions_value


def hermite(fNum, nodeNum, stopien):
    roots = (
        (-0.707107, 0.707107),
        (-1.224745, 0.0, 1.224745),
        (-1.650680, -0.534648, 0.534648, 1.650680),
        (-2.020183, -0.958572, 0.0, 0.958572, 2.020183),
    )

    coefficients = (
        (0.886227, 0.886227),
        (0.295409, 1.181636, 0.295409),
        (0.081313, 0.804914, 0.804914, 0.081313),
        (0.019953, 0.393619, 0.954309, 0.393619, 0.019953)
    )

    result = 0.0

    for i in range(nodeNum):
        result += coefficients[nodeNum - 2][i] * functions_value(roots[nodeNum - 2][i], fNum) * \
                  wielomianyHermite(stopien, roots[nodeNum - 2][i])

    return result


def wielomianyHermite(stopien, x):
    if stopien == 0:
        return 1
    elif stopien == 1:
        return 2 * x
    else:
        return 2 * x * wielomianyHermite(stopien - 1, x) - 2 * (stopien - 1) * wielomianyHermite(stopien - 2, x)
