import matplotlib.pyplot as mp
import numpy as np
from functions import functions_value


def printPlot(left, right, ax, ay, fNum):
    x = np.linspace(left, right, 1000)  # oś OX od lewego do prawego przedziału, 1000 wartości
    mp.plot(ax, ay, label="Aproksymacja")  # rysowanie wykresu
    mp.plot(ax, functions_value(ax, fNum), label="Bazowa")
    mp.ylabel("f(x)")  # nazwa osi OY
    mp.xlabel("x")  # nazwa osi OX
    mp.legend(loc="upper right")
    mp.grid(True)  # siatka
    mp.show()
