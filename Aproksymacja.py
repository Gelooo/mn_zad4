from functions import *
from Hermite import *


def silnia(n):
    if n > 1:
        return n * silnia(n - 1)
    return 1


def aproksymacja(a, b, stopien, funNum, nodeNum):
    i = a
    res = []
    ax = []
    ay = []
    while i <= b:
        ax.append(i)
        ay.append(aproksymacjaHermite(stopien, funNum, nodeNum, i))
        i += 0.01
    res.append(ax)
    res.append(ay)
    return res


def aproksymacjaErr(stopien, funNum, nodeNum, a, b):
    err = 0
    h = (abs(a - b) / (nodeNum - 1))
    i = a
    while i <= b:
        err += (functions_value(i, funNum) - aproksymacjaHermite(stopien, funNum, nodeNum, i)) * \
               (functions_value(i, funNum) - aproksymacjaHermite(stopien, funNum, nodeNum, i))
        i += h
    return err


def aproksymacjaHermite(stopien, funNum, nodeNum, x):
    sum = 0
    for i in range(0, stopien + 1):
        sum += wspolczynnikHermite(i, funNum, nodeNum) * wielomianyHermite(i, x)
    return sum


def wspolczynnikHermite(stopien, funNum, nodeNum):
    res = (1 / (np.sqrt(np.pi) * 2 ** stopien * silnia(stopien))) * hermite(funNum, nodeNum, stopien)
    return res
