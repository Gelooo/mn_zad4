from functions import functions_value
from Aproksymacja import *
from plot import printPlot
import math

# =========================== #
#   Grzegorz Kucharski 229932 #
#   Wojciech Cwynar    229856 #
#   Grupa 5                   #
# =========================== #

print("Aproksymacja oparata na wielomianach Hermite'a")
while True:
    correct = False
    function_number = 0  # numer wybranej funkcji
    while correct is not True:
        print("""\nWybierz funkcje:
        1 - 2 * x^2 - 2
        2 - sin(x)
        3 - cos(2 * x ** 2 + 1)
        4 - |x| """)
        try:
            function_number = int(input())
            if 0 < function_number < 5:
                correct = True
            else:
                print("Bledny numer funkcji")
                correct = False
        except ValueError:
            print("Bledny numer funkcji")

    correct = False

    left = float(input("Podaj lewy kraniec przedzialu: "))
    right = float(input("Podaj prawy kraniec przedzialu: "))
    nodeNum = int(input("Podaj ilosc wezlow (2,3,4,5): "))
    aproErr = float(input("Podaj oczekiwany bład aproksymacji: "))

    if aproErr == 0:
        stopien = int(input("Podaj stopien wielomianu: "))
        result = aproksymacja(left, right, stopien, function_number, nodeNum)
        print("Blad: " + str(aproksymacjaErr(stopien, function_number, nodeNum, left, right)))
        printPlot(left, right, result[0], result[1], function_number)

    else:
        stopien = 1
        err = aproksymacjaErr(stopien, function_number, nodeNum, left, right)
        while err > aproErr:
            stopien += 1
            err = aproksymacjaErr(stopien, function_number, nodeNum, left, right)

        print("Stopien wielomianu: " + str(stopien))
        print("Blad: " + str(err))
        result = aproksymacja(left, right, stopien, function_number, nodeNum)

        printPlot(left, right, result[0], result[1], function_number)